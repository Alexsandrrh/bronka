require('./scss/main.scss');
const $ = require('jquery');
const Stickyfill = require('stickyfilljs');
const { TweenMax, TimelineMax, gsap } = require('gsap');
const ScrollMagic = require('scrollmagic');
require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators');
const { ScrollMagicPluginGsap } = require('scrollmagic-plugin-gsap');
require('./scripts/jquery.mlens-1.7');

// Add polyfill for position:sticky
const stickies = document.querySelectorAll('.sticky');
Stickyfill.add(stickies);

// Init plugin scrollmagic for gsap
ScrollMagicPluginGsap(ScrollMagic, TweenMax, TimelineMax);

// Init controller
const controller = new ScrollMagic.Controller();

// 0 === Set static section animations
const section = {
  titles: document.querySelectorAll('.section__title'),
  texts: document.querySelectorAll('.section__text'),
  images: document.querySelectorAll('.section__image')
};

// Add scene for all section-items
controller.addScene(
  [...section.titles, ...section.texts, ...section.images].map(elem =>
    new ScrollMagic.Scene({
      triggerElement: elem,
      triggerHook: 0.85
    }).setClassToggle(elem, '-animate')
  )
);

// 1 === Animate logo
gsap.set('.section-main__logo', { opacity: 0, y: 120 });
gsap.to('.section-main__logo', 2, {
  opacity: 1,
  y: 0,
  ease: 'slow(0.5, 0.4, false)'
});

// 2 === Animate MainView
const mainView = document.querySelector('.section-main__view');
const mainViewRounds = document.querySelector('.section-main__view > .rounds');

controller.addScene(
  new ScrollMagic.Scene({
    triggerElement: mainView,
    triggerHook: 0.25
  }).setClassToggle(mainView, '-animate')
);

controller.addScene(
  new ScrollMagic.Scene({
    triggerElement: mainViewRounds,
    triggerHook: 0.25
  }).setClassToggle(mainViewRounds, '-animate')
);

// 3 === Counter days
const days = document.querySelector('.section-main__content__days > .days');
const counterDays = (selector, number, time = 80) => {
  let currentNumber = 0;
  const elem = document.querySelector(selector);

  const counter = setInterval(() => {
    if (currentNumber > number) {
      clearImmediate(counter);
    } else {
      elem.innerText = currentNumber;
      currentNumber += 1;
    }
  }, time);
};
let isShowDays = false;

controller.addScene(
  new ScrollMagic.Scene({
    triggerElement: days,
    triggerHook: 0.8
  })
    .on('start', () => {
      if (isShowDays) {
        isShowDays = false;
      } else {
        counterDays('#counter-days', 26, 55);
        isShowDays = true;
      }
    })
    .setClassToggle(days, '-animate')
);

// 5 === Animate Palette
const colors = document.querySelectorAll('.section-palette__color');

colors.forEach(elem => {
  new ScrollMagic.Scene({
    triggerElement: elem,
    triggerHook: 0.7
  })
    .setClassToggle(elem, '-animate')
    .addTo(controller);
});

// 6 === Animate PressPage
const sectionPressPage = {
  pages: document.querySelectorAll('.section-presspage__pages__image'),
  maps: document.querySelector('.section-presspage__maps')
};

controller.addScene(
  [...sectionPressPage.pages].map(elem => {
    return new ScrollMagic.Scene({
      triggerElement: elem,
      triggerHook: 0.5
    }).setClassToggle(elem, '-animate');
  })
);

controller.addScene(
  [sectionPressPage.maps].map(elem => {
    return new ScrollMagic.Scene({
      triggerElement: elem,
      triggerHook: 0.7
    }).setClassToggle(elem, '-animate');
  })
);

// 7 === Animate End
const sectionEndPreview = document.querySelector('.section-end__preview');
controller.addScene(
  new ScrollMagic.Scene({
    triggerElement: sectionEndPreview,
    triggerHook: 0.9
  }).setClassToggle(sectionEndPreview, '-animate')
);

// Structure map
const imageMap = $('.section-structure__map__image');

imageMap.mlens({
  imgSrc: imageMap.data('big'),
  lensCss: 'zoomer-lens',
  borderSize: '0px',
  lensShape: 'circle',
  zoomLevel: 0.7,
  responsive: true
});
