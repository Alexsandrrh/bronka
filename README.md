# Описание проекта

### Запустить проект в режиме разработке

`npm run start` или `yarn start`

### Собрать проект и запустить

`npm run start:production` или `yarn start:production`

### Просто собрать проект

`npm run build` или `yarn build`
